/*replace ugly jQuery default easing*/
jQuery.easing['jswing'] = jQuery.easing['swing'];
jQuery.extend( jQuery.easing,
              {
    def: 'easeInOutQuart',
    swing: function (x, t, b, c, d) {
        //alert(jQuery.easing.default);
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
        return -c/2 * ((t-=2)*t*t*t - 2) + b;
    }
});


$(function($){

    /*$("img.lazy").show().lazyload({
        threshold : 200,
        effect : "fadeIn",
        failure_limit : 10
    });*/

    //scroll to anchor
    $('a[href^=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - $('header.header').outerHeight(true)
                }, 1000);
                return false;
            }
        }
    });

    $('.map').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function() {
            $(this).append($('#hiddenMap'))
        },
        doOut: function() {
        },
        tolerance: -250,
        throttle: 50,
        toggleClass: 'onScreen',
        //lazyAttr: null,
        //lazyPlaceholder: '/template/img/map.jpg',
        debug: true
    });

    $('img.lazy').onScreen({
        lazyAttr: 'data-src',
        tolerance: -250,
        throttle: 50,
        doIn: function() {
            $(this).fadeIn(100);
        }
    });

    //phone mask
    $('#phone').mask('+7 (999) 999-99-99');

    //on btn click check current tour checbox
    $('[data-target=#broni]').click(function(){
        var tour = $(this).data('tour'),
            form = $('#tourform');
        form.find('[type=checkbox]').prop( "checked", false );
        form.find('[name='+tour+']').prop( "checked", true );
    });

    //datetimepicker init
    $('input#date').datetimepicker({
        locale: 'ru',
        format: 'DD.MM.YYYY',
        minDate : 'moment'
    });

    // validate form on keyup and submit
    $("#tourform").validate({
        /*submitHandler: function(form) {
            var dates = $('#date').val();
            var forms = form;
            $.ajax({
                type: 'POST',
                url: 'valid.json',
                //data: {'date': dates},
                data: {'date': dates, 'time':times},
                dataType: 'json',
                success: function (json) {
                    formSubmit();
                    // return ret;
                }
            });
        },*/
        rules: {
            name: "required",
            phone: "required",
            email: {
                required: false,
                email: true
            },
            /*tour: {
                required: function (element) {
                    var boxes = $("#tourform").find('[type=checkbox]');
                    if (boxes.filter(':checked').length == 0) {
                        return true;
                    }
                    return false;
                },
                minlength: 1
            },*/
            date: {
                required: true,
                //date:true
            },
            persons: "required",
        },
        messages: {
            name: "Обязательно для заполнения",
            phone: "Обязательно для заполнения",
            email: "Неправильный формат email",
            /*tour: "Выберите тур",*/
            date: "Неправильный формат даты",
            persons: "Кол-во гостей не выбрано"
        }
    });

    //formSubmit
    $("#tourform").submit(function(){
        var form = $(this),
            action = form.attr('action'),
            resultblock = form.find('.alert'),
            msg = form.serialize();
        $.ajax({
            type: 'POST',
            url: action,
            data: msg,
            success: function(data) {
                form.slideToggle(function(){
                    form.html(data).fadeIn();
                });
                form.slideToggle(function(){
                    form.html(data).fadeIn();
                });
            },
            error: function(xhr, str){
                resultblock.html('Возникла ошибка: ' + xhr.responseCode).slideDown();
            }
        });
        return false;
    });


});



